cowboy
======

Cowboy is a builder for projects comprising multiple modules in independent git repositories.

Copyright (C) 2022 by Iain Nicholson. iain.j.nicholson@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

Introduction
------------

Cowboy is a set of scripts written in bash on Slackware Linux for building
projects comprising several source modules in independent git repositories.

Cowboy automates the familiar sequence:

    git clone
    make
    make install
    ldconfig

Cloning and building are done as an unprivileged user. Installation is
optionally performed as root via sudo, if configured.

**Note that you should only install software that you trust, and only people you
trust should install software on your system.**

Cowboy consists of the following scripts, which must be in your PATH and have
execute permission to use:

    cowboy
    cowboyLib
    cowboyBuildSequence
    cowboyUninstallSequence
    datestamp

cowboy is the top level script and contains help:

    cowboy --help

Dependencies are listed in a plain text definition file and proto_project is
provided as an example. proto_project contains the four public C projects in
Captain Spaghetti's Code Shed (https://gitlab.com/spaghetti-shed). To build:

    cowboy proto_project ~/cowboy_builds

This should fail, since it doesn't install the dependencies as they are built!

If you feel lucky, you can run with the dangerous option to perform the make
install phase (assuming you have your sudoers file set up):

    cowboy --dangerous proto_project ~/cowboy_builds

Cowboy also monitors the target (installation) directory (currently hard-coded
to /usr/local) producing sorted lists of all files and directories before and
after installation (make install, cowboyInstall) in the build directory of the
form cowboy_delta-YYYYMMDD-HHMMSS.before and cowboy_delta-YYYYMMDD-HHMMSS.after
and then uses diff to output the changes to cowboy_delta-YYYYMMDD-HHMMSS

Hopefully this will show which files and directories have been created and/or
removed during the process. Note that it does not show those whose contents have
changed.

The interface subdirectory contains very small scripts which implement the
interface to the outside world that can be implemented to let cowboy build non-
standard or otherwise packages.

Recursion
---------

Cowboy builds dependencies recursively. If a git repository contains a cowboy
dependency file called project.cow, it will recurse using project.cow as its
input. Cowboy always builds the dependencies first, and only once, so multiple
dependencies containig similar dependency trees can be used and will not cause
unnecessary rebuilds or loops.

Configuring sudo
----------------

Use visudo to edit your sudo configuration file and add a line as follows to
permit user fred to execute the commands in cowInstall, cowLdconfig and
cowUninstall:

    fred ALL=(root) NOPASSWD:/usr/bin/make install, /usr/bin/make uninstall, /sbin/ldconfig -v

This allows user "fred" to execute precisely the three commands listed as root
(super user) without entering a password. **Do you trust fred?**

Interface to Build Systems
--------------------------

See interface/README for a brief description of how cowboy interfaces with the packages it builds.


TO-DO:
* Better documentation
* Better abstraction
* Different build sequences
* Support for other SCM systems
* File system deltas per project
* Changed files

