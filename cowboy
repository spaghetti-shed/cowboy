#!/bin/bash
#
# Cowboy - Build a project containing modules in multiple independent git
#          repositories.
# Fri Jun  3 13:20:16 BST 2022
#
# Copyright 2022 by Iain Nicholson. <iain.j.nicholson@gmail.com>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# Cowboy is a set of scripts written in bash on Slackware Linux for building
# projects comprising several source modules in intependent git repositories.
#
# Modification history:
# 2022-06-03 Initial creation.
# 2022-06-09 Add dangerous (make install) option.
# 2022-06-12 License under Apache-2.0 License (see LICENSE-2.0).
#            Create interface to abstract out build process steps.
# 2022-06-13 Only build in the current directory if it has something to build.
# 2022-06-14 Add uninstall target.
# 2022-06-15 Check whether helper scripts can be found in PATH.
#            Catalogue the destination directory before and after the run and
#            produce a delta file showing the files added and removed.
# 2022-06-20 Fix protocol bug.
#            Fix recursion.
#            Fix --local behaviour.
#
MYNAME=`basename $0`

#
# Include library functions
#
. cowboyLib

usage()
{
    echo "${MYNAME}: Build a project containing multipe source modules in multiple independent git repositories."
    echo
    echo "Usage:"
    echo "    ${MYNAME} [<option>] <project_file> [<build_dir>]"
    echo
    echo "Where <project_file> is the name of the project file containing the prroject name and dependencies and <build_dir> is the directory under which to perform the build. If the build directory is not specified, ${MYNAME} defaults to building in the current directory."
    echo
    echo "Options:"
    echo "  -h | --help       Show this help text."
    echo "  -l | --local      Local build. Reuse previously downloaded source, performing a 'make clean' on each package first. The datestamp of the build directory must be supplied as the last command line argument. Does not install."
    echo "  -d | --dangerous  Do a 'sudo make install' followed by a 'sudo ldconfig -v' for each module."
    echo "  -u | --uninstall  Do a 'sudo make uninstall' for each module."
    echo
    echo "Examples:"
    echo
    echo "  cowboy proto_projet"
    echo
    echo "Download and build the packages listed in the proto_project file under the current directory in a subdirectory called proto_project-YYYYMMDD-HHMMSS where YYYYMMDD-HHMMSS is the current date and time."
    echo
    echo "  cowboy proto_project ~"
    echo
    echo "Download and build the packages listed in the proto_project file under the user's home directory in a subdirectory called proto_project-YYYYMMDD-HHMMSS where YYYYMMDD-HHMMSS is the current date and time."
    echo
    echo "  cowboy -l proto_project ~ YYYYMMDD-HHMMSS"
    echo
    echo "Rebuild the packages (without downloading) listed in the proto_project file under the user's home directory in a subdirectory called proto_project-YYYYMMDD-HHMMSS where YYYYMMDD-HHMMSS is the date and time appended to the build directory name. In this case, make clean is performed on each target prior to make."
    echo
    echo "If a dependecy contains its own cowboy dependency file and it is called project.cow cowboy will recurse. This way a project can depend on subprojects. If the subprojects also depend on subprojects, any common dependencies are only cloned and built once."
    echo
    exit 2
}

#
# Settings
#
export LOCAL=0
export DANGEROUS=0

#
# Process command line options.
#
process_opts()
{
    if [ $# -ge 1 ]
    then
        PARAM=$1
        if [ ${PARAM:0:2} == "--" ]
        then
            case ${PARAM} in
                "--local" )
                    export LOCAL=1
                    shift
               ;;
                "--dangerous" )
                    export DANGEROUS=1
                    shift
               ;;
                "--uninstall" )
                    export UNINSTALL=1
                    shift
               ;;
               "--help" )
                   usage
               ;;
               * )
                   abort "Unrecognised option ${PARAM}"
                ;;
            esac
        elif [ ${PARAM:0:1} == "-" ]
        then
            case ${PARAM} in
                "-l" )
                    export LOCAL=1
                    shift
               ;;
                "-d" )
                    export DANGEROUS=1
                    shift
               ;;
                "-u" )
                    export UNINSTALL=1
                    shift
               ;;
               "-h" )
                   usage
               ;;
               * )
                   abort "Unrecognised option ${PARAM}"
                ;;
            esac
        fi
    fi
    PROJECT_FILE=$1
    if [ -z "${COWBOY_ROOT}" ]
    then
        export COWBOY_ROOT=$2
    fi
    if [ "${LOCAL}" == "1" ]
    then
        LAST=$3
    fi
}

#
# Cowboy needs to know where all its helper scripts are.
#
check_helpers()
{
    for helper in cowboyBuildSequence \
                  cowboyLib \
                  cowboyUninstallSequence \
                  datestamp
    do
        which "${helper}" 2>&1 | grep '^which: no' > /dev/null
        if [ $? == 0 ]
        then
            abort "${helper} not found. Please update PATH."
        fi
    done
}

#
# Start here.
#
export COWBOY_INSTALL_DEST=/usr/local
check_helpers

export CURRENT=`datestamp`

process_opts "$@"

if [ -z "${PROJECT_FILE}" ]
then
    abort "Please supply a project file."
fi

# Root for creating project directories.
if [ -z "${COWBOY_ROOT}" ]
then
    info "No root build directory specified. Using current."
    export COWBOY_ROOT=`pwd`
fi

#
# How many times have we recursed?
#
if [ -z "${COWBOY_RECURSE_LEVEL}" ]
then
    export COWBOY_RECURSE_LEVEL=0
fi

#
# A file to list the filesystem changes.
# Note this must be constant over recursion.
#
if [ -z "${COWBOY_DELTA}" ]
then
    export COWBOY_DELTA=${COWBOY_ROOT}/cowboy_delta-${CURRENT}
    date > ${COWBOY_DELTA}.before
    cowboyIndex ${COWBOY_INSTALL_DEST} >> ${COWBOY_DELTA}.before
fi

((COWBOY_RECURSE_LEVEL+=1))

if [ ! -e "${PROJECT_FILE}" ]
then
    info "No project file: ${PROJECT_FILE} found."
    info "Defaulting to non-recursive standard build."
else
cat ${PROJECT_FILE} | \
    grep -v '^ *#' | \
    grep -v '^ *$' | \
    sed 's/#.*$//' | (
PROJECT_NAME=""
while read line
do
    echo ${line}
    set -- ${line}
    if [ $1 == "name" ]
    then
        if [ -z "${PROJECT_NAME}" ]
        then
            if [ $# -ne 2 ]
            then
                abort "name expects an argument."
            else
                if [ "${LOCAL}" == "1" ]
                then
                    PROJECT_NAME="$2-${LAST}"
                else
                    PROJECT_NAME="$2-${CURRENT}"
                fi
                #
                # Only set this the first time if we are recursing.
                #
                if [ -z "${PROJECT_ROOT}" ]
                then
                    export PROJECT_ROOT="${COWBOY_ROOT}/${PROJECT_NAME}"
                fi
                info "Building in ${PROJECT_ROOT}"
                mkdir -p "${PROJECT_ROOT}"
            fi
        else
            abort "name already defined."
        fi
    fi
    if [ $1 == "dep" ]
    then
        if [ -z "${PROJECT_NAME}" ]
        then
            abort "No project name defined,"
        fi
        if [ $# -ne 4 ]
        then
            abort "dep expects 3 arguments."
        fi
        PROTOCOL=$2
        REMOTE_HOST=$3
        MODULE_PATH=$4
        #echo "PROTOCOL: ${PROTOCOL}"
        #echo "REMOTE_HOST: ${REMOTE_HOST}"
        #echo "MODULE_PATH: ${MODULE_PATH}"
        if [ "${PROTOCOL}" == "git" ]
        then
            MODULE_URL="git@${REMOTE_HOST}:${MODULE_PATH}"
        elif [ "${PROTOCOL}" == "https" ]
        then
            MODULE_URL="https://${REMOTE_HOST}/${MODULE_PATH}"
        else
            abort "Unknown protocol ${PROTOCOL}."
        fi
        MODULE_DIR=`basename ${MODULE_PATH} .git`
        pushd "${PROJECT_ROOT}" > /dev/null
            if [ "${LOCAL}" == 0 ]
            then
                if [ -e "${MODULE_DIR}" ]
                then
                    echo "${MODULE_DIR} already pulled."
                else
                    git clone "${MODULE_URL}"
                    if [ $? -ne 0 ]
                    then
                        abort "Failed to git clone ${MODULE_URL}."
                    fi
                fi
            fi
            pushd ${MODULE_DIR} > /dev/null
                if [ "${UNINSTALL}" == "1" ]
                then
                    cowboyUninstallSequence "${MODULE_DIR}"
                else
                    if [ "${LOCAL}" -ne 0 ]
                    then
                        if [ -e project.cow ]
                        then
                            cowboy --local project.cow "${PROJECT_ROOT}" 
                        else
                            cowboyBuildSequence "${MODULE_DIR}" "${LOCAL}" "${DANGEROUS}"
                        fi
                    else
                        if [ -e "cowboy.done" ]
                        then
                            echo "${MODULE_DIR} already built."
                        else
                            if [ -e project.cow ]
                            then
                                if [ "${DANGEROUS}" == 1 ]
                                then
                                    cowboy --dangerous project.cow "${PROJECT_ROOT}" 
                                else
                                    cowboy project.cow "${PROJECT_ROOT}" 
                                fi
                            else
                                cowboyBuildSequence "${MODULE_DIR}" "${LOCAL}" "${DANGEROUS}"
                            fi
                        fi
                    fi
                fi
            popd > /dev/null
            echo "Done."
        popd > /dev/null
    fi
done
)
fi

#
# We have run out of things in the dependencies file.
# Do we have something to build in the current directory?
# 
if [ -f ./Makefile ] || [ -f ./cowMake ]
then
    echo "Building current in `pwd`"
    MODULE_PATH=`basename $PWD`
    if [ "${LOCAL}" == "1" ]
    then
        echo "Cleaning ${MODULE_PATH}."
        if [ -e ./cowClean ]
        then
            ./cowClean
        else
            make clean
        fi
        if [ $? -ne 0 ]
        then
            abort "Cleaning ${MODULE_PATH} failed."
        fi
    fi

    MODULE_DIR=`basename ${MODULE_PATH}`
    if [ "${UNINSTALL}" == 1 ]
    then
        cowboyUninstallSequence "${MODULE_DIR}"
    else
        cowboyBuildSequence "${MODULE_DIR}" "${LOCAL}" "${DANGEROUS}"
    fi
fi

((COWBOY_RECURSE_LEVEL-=1))

if [ ${COWBOY_RECURSE_LEVEL} == 0 ]
then
    date > ${COWBOY_DELTA}.after
    cowboyIndex ${COWBOY_INSTALL_DEST} >> ${COWBOY_DELTA}.after
    diff ${COWBOY_DELTA}.before ${COWBOY_DELTA}.after > ${COWBOY_DELTA}
fi

echo "Done."

