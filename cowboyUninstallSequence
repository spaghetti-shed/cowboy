#!/bin/bash
#
# cowboyUninstallSequence - Uninstall for cowboy.
# Tue Jun 14 21:29:30 BST 2022
#
# Copyright 2022 by Iain Nicholson. <iain.j.nicholson@gmail.com>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# Cowboy is a set of scripts written in bash on Slackware Linux for building
# projects comprising several source modules in intependent git repositories.
#
# Modification history:
# 2022-06-14 Extracted from cobwboyBuildSequence.
#
MYNAME=`basename $0`

. cowboyLib

usage()
{
    echo "${MYNAME}: Execute the uninstall targets from a project in current directory using a pre-determined sequence of commands."
    echo
    echo "Usage:"
    echo "    ${MYNAME} <module>"
    echo
    exit 2
}

process_opts()
{
    if [ $# -ne 1 ] || [ $1 == "-h" ] || [ $1 == "--help" ]
    then
        usage
    fi
}


process_opts "$@"


MODULE_DIR=$1

echo "Uninstalling ${MODULE_DIR}."
if [ -e ./cowUninstall ]
then
    ./cowUninstall
else
    sudo make uninstall
fi
if [ $? -ne 0 ]
then
    abort "Uninstalling ${MODULE_DIR} failed."
fi
rm -f ./cowboy.done

