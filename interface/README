Copyright (C) 2022 by Iain Nicholson <iain.j.nicholson@gmail.com>

12th June 2022
--------------

This directory contains prototype scripts that implement a bare minimum
interface to cowboy which can be copied into a project's top-level directory
to implement that interface.

        WARNING        WARNING        WARNING
        -------        -------        -------

WARNING: cowInstall, cowLdconfig and cowUninstall use sudo internally to run
commands as root. Make sure that you trust the code that you are compiling and
installing!

You may copy and modify the following files in this subdirectory to your
heart's content:

    cowConfigure (to perform the ./configure or equivalent)
    cowMake      (to perform the make step or equivalent)
    cowInstall   (to perform the "make install" step or equivalent as root via
sudo)
    cowLdconfig  (to perform the "/sbin/ldconfig -v" step or equivalent as
root via sudo)
    cowUninstall (to perform the "make uninstall" step or equivalent as root
via sudo)
    cowClean     (to perform the "make clean" step or equivalent)
    cowDistclean (to perform the "make distclean" step or equivalent)

Each script must return 0 on success or non-zero on failure.

Configuring sudo
----------------

Use visudo to edit your sudo configuration file and add a line as follows to
permit user fred to execute the commands in cowInstall, cowLdconfig and
cowUninstall:

fred ALL=(root) NOPASSWD:/usr/bin/make install, /usr/bin/make uninstall,
/sbin/ldconfig -v

This allows user "fred" to execute precisely the three commands listed as root
(super user) without entering a password. Do you trust fred?

